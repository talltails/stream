# Icecast 2 stream

Environment variables for configuring the service:
```
SOURCEPW
RELAYPW
ADMINUSER
ADMINPW
HOSTNAME
PORT

AUTODJMOUNT
AUTODJSTREAMNAME
AUTODJUSER
AUTODJPASS

STREAMMOUNT
STREAMNAME
STREAMPUBLIC
STREAMUSER
STREAMPASS
```
