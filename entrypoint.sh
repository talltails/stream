#!/usr/bin/env sh

if [ "$1" = 'icecast2' ]; then

  if [ ! -d 'icecast.xml' ]; then
    tee icecast.xml <<EOF >/dev/null
<icecast>
    <location>Earth</location>
    <admin>icemaster@localhost</admin>
    <limits>
        <clients>100</clients>
        <sources>2</sources>
        <queue-size>524288</queue-size>
        <client-timeout>30</client-timeout>
        <header-timeout>15</header-timeout>
        <source-timeout>10</source-timeout>
        <burst-on-connect>1</burst-on-connect>
        <burst-size>65535</burst-size>
    </limits>

    <authentication>
        <source-password>$SOURCEPW</source-password>
        <relay-password>$RELAYPW</relay-password>
        <admin-user>$ADMINUSER</admin-user>
        <admin-password>$ADMINPW</admin-password>
    </authentication>

    <hostname>$HOSTNAME</hostname>

    <listen-socket>
        <port>$PORT</port>
    </listen-socket>

    <http-headers>
        <header name="Access-Control-Allow-Origin" value="*" />
    </http-headers>
    
    <mount>
        <mount-name>$AUTODJMOUNT</mount-name>
        <stream-name>$AUTODJSTREAMNAME</stream-name>
        <public>0</public>
        <hidden>1</hidden>
        <username>$AUTODJUSER</username>
        <password>$AUTODJPASS</password>
    </mount>
    <mount>
        <mount-name>$STREAMMOUNT</mount-name>
        <fallback-mount>$AUTODJMOUNT</fallback-mount>
        <fallback-override>1</fallback-override>
        <stream-name>$STREAMNAME</stream-name>
        <public>$STREAMPUBLIC</public>
        <username>$STREAMUSER</username>
        <password>$STREAMPASS</password>
    </mount>

    <fileserve>1</fileserve>

    <paths>
        <basedir>/usr/share/icecast2</basedir>
        <logdir>/var/log/icecast2</logdir>
        <webroot>/usr/share/icecast2/web</webroot>
        <adminroot>/usr/share/icecast2/admin</adminroot>
        <alias source="/" destination="/status.xsl"/>
    </paths>

    <logging>
        <accesslog>access.log</accesslog>
        <errorlog>error.log</errorlog>
        <loglevel>3</loglevel>
        <logsize>10000</logsize>
    </logging>

    <security>
        <chroot>0</chroot>
        <changeowner>
            <user>icecast2</user>
            <group>nogroup</group>
        </changeowner>
    </security>
</icecast>
EOF
  fi
  exec icecast2 "$@"
fi

exec "$@"
